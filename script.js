/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        }
    ]
}
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    console.log("Loading...");
    loadMostPopularCoursesToWebsite();
    loadTrendingCoursesToWebsite();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Hàm tải dữ liệu most popular course vào web
function loadMostPopularCoursesToWebsite() {
    var vContentHtml = "";
    for (var i = 0; i < gCoursesDB.courses.length; i++ ) {
        if (gCoursesDB.courses[i].isPopular == true) {
            vContentHtml += `<div class="col-sm-3">
            <div class="card course-card">
              <div class="card-header">
                <img class="img-card-header" title="Angular" src="${gCoursesDB.courses[i].coverImage}" alt="Angular">
              </div>
              <div class="card-body">
                <p class="card-text text-primary">How to easily create a website with Angular</p>
                <div class="row ml-1">
                  <i class="fa-solid fa-clock fa fa-light"></i>
                  <p class="text-muted course-info">${gCoursesDB.courses[i].duration}</p>
                  <p class="text-muted course-info">${gCoursesDB.courses[i].level}</p>
                </div>
                <div class="row ml-1">
                  <p style="font-weight:bold;">$${gCoursesDB.courses[i].discountPrice}</p>
                  <p class="text-muted price-course">$${gCoursesDB.courses[i].price}</p>
                </div>
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-sm-2">
                    <img class="img-teacher" title="Course Teacher" src="${gCoursesDB.courses[i].teacherPhoto}"
                      alt="Morris Mccoy">
                  </div>
                  <div class="col-sm-8">
                    <p class="text-muted text-center teacher-name"><small>${gCoursesDB.courses[i].teacherName}</small></p>
                  </div>
                  <div class="col-sm-2">
                    <i class="text-muted fa-solid fa-bookmark"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>`
        }
      
    }
    $("#most-popular-cards").html(vContentHtml);
}// Hàm tải dữ liệu most popular course vào web
function loadTrendingCoursesToWebsite() {
    var vContentHtml = "";
    for (var i = 0; i < gCoursesDB.courses.length; i++ ) {
        if (gCoursesDB.courses[i].isTrending == true) {
            vContentHtml += `<div class="col-sm-3">
            <div class="card course-card">
              <div class="card-header">
                <img class="img-card-header" title="Angular" src="${gCoursesDB.courses[i].coverImage}" alt="Angular">
              </div>
              <div class="card-body">
                <p class="card-text text-primary">How to easily create a website with Angular</p>
                <div class="row ml-1">
                  <i class="fa-solid fa-clock fa fa-light"></i>
                  <p class="text-muted course-info">${gCoursesDB.courses[i].duration}</p>
                  <p class="text-muted course-info">${gCoursesDB.courses[i].level}</p>
                </div>
                <div class="row ml-1">
                  <p style="font-weight:bold;">$${gCoursesDB.courses[i].discountPrice}</p>
                  <p class="text-muted price-course">$${gCoursesDB.courses[i].price}</p>
                </div>
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-sm-2">
                    <img class="img-teacher" title="Course Teacher" src="${gCoursesDB.courses[i].teacherPhoto}"
                      alt="Morris Mccoy">
                  </div>
                  <div class="col-sm-8">
                    <p class="text-muted text-center teacher-name"><small>${gCoursesDB.courses[i].teacherName}</small></p>
                  </div>
                  <div class="col-sm-2">
                    <i class="text-muted fa-solid fa-bookmark"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>`
        }
      
    }
    $("#trending-cards").html(vContentHtml);
}

